#Export AD Users to CSV for inport on WHD

Import-Module ActiveDirectory
Get-ADUser -Filter * -Properties * |
 Select -Property givenName,sn,sAMAccountName,mail | 
 Export-CSV "C:\var\ADUsers.csv" -NoTypeInformation -Encoding UTF8